# Start from a simple python image
FROM python:3.7

# Install frictionless data toolchain
RUN pip install dataflows
#RUN npm install -g data-cli